<?php
/**
 *
 * Tabbed Smilies. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, Lupe Fluffington
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

//FIXME Bring code up to phpBB standard. Use language variables in template.

namespace fluffington\tabbedsmilies\event;

use phpbb\db\driver\driver;
use phpbb\db\driver\driver_interface;
use phpbb\notification\manager;
use phpbb\template\template;
use phpbb\config\config;
use phpbb\path_helper;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class main_listener implements EventSubscriberInterface
{
	public static function getSubscribedEvents()
	{
		return [
			'core.user_setup'						=> 'load_language_on_setup',
			'core.generate_smilies_modify_rowset'	=> 'generate_tabs',
		];
	}

	protected $db;
	protected $config;
	protected $table;

	public function __construct(template $template, driver_interface $db, config $config, path_helper $path_helper)
	{
		$this->template = $template;
		$this->db = $db;
		$this->config = $config;
		global $table_prefix;
		$this->table = $table_prefix . "smilies_tabs";
		$this->smilies = $table_prefix . "smilies";
	}

	public function load_language_on_setup($event)
	{
		$lang_set_ext = $event['lang_set_ext'];
		$lang_set_ext[] = [
			'ext_name' => 'fluffington/tabbedsmilies',
			'lang_set' => 'common',
		];
		$event['lang_set_ext'] = $lang_set_ext;
	}

	public function generate_tabs($event)
	{
		//Don't need your smilies. We'll do it ourselves. This also turns off the smiley box.
		$event['smilies'] = [];

		$tabs = [];
		$query = "select * from $this->table order by tab_order";
		$result = $this->db->sql_query($query);
		while ($row = $this->db->sql_fetchrow($result))
		{
			$tabs[$row['id']] = [
					'name'		=> $row['name'],
					'id'		=> $row['id'],
					'smilies'	=> [],
				];
		}
		$leftovers = [];

		//FIXME Replace this with phpBB's own fetch smiley event. Saves like 0.0005 seconds.
		$result = $this->db->sql_query("select * from $this->smilies where tab_id != 0 order by tab_id,tab_order");
		while($row = $this->db->sql_fetchrow($result))
		{
			$tabs[$row["tab_id"]]['smilies'][] = $row;
		}
		$this->template->assign_var("tabs",$tabs);
	}
}
