var didSort = false;
var rename_store = {};
var delete_store = [];

var langu = {
	"delete": $(".deletebutton").attr("title"),
	"rename": $(".renamebutton").attr("title"),
	"new": "New",
	"newBox": $(".newbutton").attr("title")
};


$( window ).on("beforeunload",function(){
	if(didSort){
		return "Are you sure?";
	}
});

$("#resetButton").on("click", function(){
	didSort = false;
	window.location.reload();
});

$("#fluffington_tabbedsmilies_acp").on("submit", function(event){
	//FIXME Need to add form validation here and in the php script
	var ob = ['tab-0'].concat($(".outerbox").sortable("toArray"));
	var allTabs = {};
	for(const tab of ob){
		if(tab == "newbox"){
			continue;
		}
		allTabs[tab] = $('#'+tab).sortable("toArray");
	}
	$('#updated_sorting')[0].value = JSON.stringify(allTabs);
	$('#updated_names')[0].value = JSON.stringify(rename_store);
	$('#deleted_tabs')[0].value = JSON.stringify(delete_store);
	return false;
});

$("#outerbox").sortable({
	items: ".outersort",
	tolerance: "pointer",
	placeholder: "placeholder",
	cursor: "grabbing",
	stop: function(){
		didSort = true;
	}
});

$(".innerbox").sortable({
	connectWith: ".innerbox",
	placeholder: "miniholder",
	items:"> img",
	cursor: "grabbing",
	activate: function( event, ui){
		$(ui['placeholder']).attr("src","../images/spacer.gif");
		$(ui['placeholder']).css("width",$(ui['item']).css("width"));
		$(ui['placeholder']).css("height",$(ui['item']).css("height"));
	},
	stop: function(){
		didSort = true;
	}
});

function updateButtons(){
	$(".rename_box").off("keydown");
	$(".rename_box").off("blur");
	$(".deletebutton").off("click");
	$(".renamebutton").off("click");

	$(".renamebutton").on("click",function(){
		$(this).parent().css("display","none");
		$(this).parent().next().css("display","unset");
		$(this).parent().next().focus();
	});
	$(".rename_box").on("keydown",function(event){
		if ( event.which == 13 ) {
			h2box = $(this).parent();
			h2box.find(".h2_text").text($(this)[0].value);
			h2box.find("input").css("display","none");
			h2box.find("span").css("display","unset");
			rename_store[$(this).attr('data-id')] = $(this)[0].value;
		}
	});
	$(".rename_box").on("blur",function(){
		h2box = $(this).parent();
		h2box.find(".h2_text").text($(this)[0].value);
		h2box.find("span").css("display","unset");
		h2box.find("input").css("display","none");
		rename_store[$(this).attr('data-id')] = $(this)[0].value;
	});

	$(".deletebutton").on("click",function(){
		//FIXME Replace with phpBB native confirm box
		result = window.confirm("Are you sure you want to delete this tab?");
		if(result){
			if(!$(this).attr('data-id').startsWith("newtab")){
				delete_store.push($(this).attr('data-id'));
			}
			$innerbox = $(this).parentsUntil(".outerbox");
			$("#tab-1").append($innerbox.find(".sortsmiley"));
			$innerbox.parentsUntil(".outerbox").remove();
			didSort = true;
		}
	});

	$(".newbutton").on("click",function(){
		var id = $(this).attr("data-id");
		rename_store["newtab-"+id] = langu['new'];
		$("#newbox").replaceWith(`
			<div class="innerbox outersort" id="newtab-${id}">
				<h2>
					<span><span class="h2_text">${langu['new']}</span> <i class="icon fas fa-cog renamebutton" title="${langu['rename']}"></i> <i class="icon far fa-times-circle deletebutton" data-id="newtab-${id}" title="${langu['delete']}"></i></span>
					<input autocomplete="off" type="text" class="rename_box" data-id="newtab-${id}" value="${langu['new']}" />
				</h2>
			</div>`);
		id++;
		$("#outerbox").append(`
			<div class="newbox outersort" id="newbox" style="position:relative">
				<h2>${langu['newBox']}</h2>
				<div class="buttonbox">
					<i class="icon fas fa-plus-square newbutton" data-id="${id}" title="${langu['newBox']}"></i>
				</div>
			</div>`);
		$("#outerbox").sortable("refresh");
		$(".innerbox").sortable({
			connectWith: ".innerbox",
			placeholder: "miniholder",
			items:"> img",
			cursor: "grabbing",
			activate: function( event, ui){
				$(ui['placeholder']).attr("src","../images/spacer.gif");
				$(ui['placeholder']).css("width",$(ui['item']).css("width"));
				$(ui['placeholder']).css("height",$(ui['item']).css("height"));
			},
			stop: function(){
				didSort = true;
			}
		});
		didSort = true;
		updateButtons();
	});
}
updateButtons();

phpbb.addAjaxCallback('fluffington.didSort', function(){
	didSort = false;
	//FIXME Need to reload the page or fix repeated new tabs
});
